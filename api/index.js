const express = require('express');
const session = require('express-session');
const bp = require('body-parser');

const app = express();
const router = express.Router();

const host = process.env.HOST || '0.0.0.0'
const port = process.env.PORT || 3000

app.set('port', port);

app.use(bp.urlencoded({
    extended: true
}));

app.use(bp.json());

app.use(session({
    secret: 'tokesecretodasessao',
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge: 60000 }
}));

app.use('/', router);

router.get('/api/login', (req, res) => {
    return res.json({
        msg: 'Funcionando'
    });
});

router.post('/api/login', (req, res) => {
    const { busuario } = req.body;

    if(req.body.usuario === 'deyvison' && req.body.senha === '123') {
        req.session.user = busuario;
        return res.status(200).json(req.body);
    }
    res.status(401).json({ msg: 'Informações inválidas'});
});


router.post('/api/logout', (req, res) => {
    delete req.session.authUser
    res.json({ ok: true })
});

app.listen(port, host)

console.log('Escutando servidor em http://' + host + ':' + port)