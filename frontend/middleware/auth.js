module.exports = ({ store, error }) => {
    if(!$store.state.user) {
        error({
            message: 'Você não está logado',
            statusCode: 401
        });
    }
}