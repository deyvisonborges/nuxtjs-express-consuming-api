export default {
    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/proxy',
        '@nuxtjs/dotenv'
    ],
    proxy: {
        '/api': {
            target: 'http://127.0.0.1:9999',
        }
    },
}