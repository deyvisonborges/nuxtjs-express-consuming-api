import axios from 'axios';

export const state = () => ({
    user: null
})

export const mutations = {
    SET_USER(state, user) {
        state.user = user
    }
}

export const actions = {
    nuxtServerInit ({ commit }, { req }) {
        if (req.session && req.session.user) {
            commit('SET_USER', req.session.user)
        }
    },
    async login ({ commit }, { usuario, senha }) {
        try {
            const { data } = await axios.post('/api/login', { usuario, senha });
            // const { data } = await axios.post(process.env.API_HOST + 'api/login', { usuario, senha });
            console.log(`${process.env.API_HOST + 'api/login'}`)
            commit('SET_USER', data);
        } catch (error) {
            if (error.response && error.response.status === 401) {
                throw new Error(error.response.data.msg)
            }
            return error;
        }
    },
    async logout ({ commit }) {
        await axios.post('/api/logout')
        commit('SET_USER', null)
    }
}